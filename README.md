# QuizHub project

[Krystyna Varenyk](mailto:kristinkavarenyk@gmail.com) and [Timur Porokhnia](mailto:timur.porokhnia@smcebi.edu.pl) project for *"Programming as a team"* course for Applied Informatics studies at University of Silesia.

Fairly simple web application for creating and answering quizzes.

Written using:

1. [Vue.js](https://vuejs.org/)
2. [Vuex](https://vuex.vuejs.org/)
3. [Vue-Router](https://router.vuejs.org/)
4. [Axios](https://github.com/axios/axios)

